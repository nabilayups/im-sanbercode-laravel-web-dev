<?php

require_once('animal.php');
require_once('Ape.php');
require_once("Frog.php");


// class animal
$sheep = new Animal("shaun");

echo "Name : ". $sheep->name . "<br>";  // "shaun"
echo "Legs : ". $sheep->legs . "<br>";  // 4
echo "Cold blooded : ". $sheep->cold_blooded . "<br>"; // "no"


//class frog
$kodok = new Frog("buduk");

echo "<br> Name : ". $kodok-> name . "<br>";
echo "Legs : ". $kodok->legs . "<br>";  // 4
echo "Cold blooded : ". $kodok->cold_blooded . "<br>"; // "no"
echo "Jump : ". $kodok->jump() . "<br>" ; // "hop hop"


//class ape
$sungokong = new Ape("kera sakti");

echo "<br>Name : ". $sungokong->name . "<br>"; 
echo "Legs : ". $sungokong->legs . "<br>";  // 2
echo "Cold blooded : ". $sungokong->cold_blooded . "<br>"; // "no"
echo "Yell : ". $sungokong->yell() . "<br>"; // "Auooo"

?>
