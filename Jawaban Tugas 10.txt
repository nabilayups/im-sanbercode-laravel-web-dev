Jawaban Tugas 10 Nabila

1. Membuat database
CREATE DATABASE myshop;


2. Membuat table ke dalam database
a. Table users
   CREATE TABLE users (
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT NULL,
    email varchar(255)NOT NULL,
    password varchar(255) NOT NULL
    );

b. Table categories
   CREATE TABLE categories (
    id	int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255)
    );

c. Table items
   CREATE TABLE items (
    id	int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    description	varchar(255),
    price int(12),
    stock int(8),
    category_id	int(8) NOT NULL,
    FOREIGN KEY(category_id) REFERENCES categories(id)
    );


3. Menambahkan data ke dalam table 
a. users :
INSERT INTO users (name, email, password) VALUES("John Doe", "john@doe.com","john123");
INSERT INTO users (name, email, password) VALUES("Jane Doe", "jane@doe.com","jenital123");

b. categoies :
INSERT INTO categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

c. items :
INSERT INTO items(name, description, price, stock, category_id) 
VALUES  ("Sumsang b50","hape keren dari merek sumsang","4000000","100","1"),
	("Uniklooh","baju keren dari brand ternama","500000","50","2"),
        ("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");


4.Mengambil data dari database
a. users :
SELECT id, name, email from users;

b. items :
- SELECT * FROM items WHERE price > 1000000;
- SELECT * FROM items WHERE name LIKE  'uniklo%';

c. categories :
SELECT items.*, categories.name FROM items INNER JOIN categories on items.category_id = categories.id;


5. Mengubah data dari database
UPDATE items set price=2500000 WHERE name='sumsang b50';