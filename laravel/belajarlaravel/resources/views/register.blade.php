@extends('layout.master')
@section('title')
Buat Acoount Baru
@endsection

@section('sub-title')
    <h4>Sign Up Form</h4>

@endsection


@section('content')
    

    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        <!--Dropdown-->
        <label>Nationality:</label>
        <select name="nationality"><br>
            <option value="ind">Indonesia</option><br>
            <option value="thai">Thailand</option><br>
            <option value="mys">Malaysia</option>
        </select><br><br>
        <!--Checkbox-->
        <label>Language Spoken:</label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <!--textarea utk inputan lebih besar-->
        <label>bio</label><br>
        <textarea name="message" rows="8" cols="28"></textarea>
        <br><br>
        <input type="submit" value="Sign Up"><br>


    </form>
    @endsection