@extends('layout.master')
@section('title')
    Halaman Detail Data Pemain
@endsection
@section('sub-title')
    Detail Data Pemain
@endsection

@section('content')
    <h1>{{$casts->nama}}</h1>
    <p>Umur {{$casts->umur}}</p>
    <p>{{$casts->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection