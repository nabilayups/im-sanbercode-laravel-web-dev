@extends('layout.master')
@section('title')
    Halaman Data Pemain
@endsection
@section('sub-title')
    Data Pemain
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Pemain</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>
              <form action="/cast/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
            </td>
          </tr>
      @empty
          <tr>
            <td>Data Pemain Kosong</td>
          </tr>
      @endforelse
    </tbody>
  </table>
  
@endsection