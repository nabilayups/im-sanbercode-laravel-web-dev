@extends('layout.master')
@section('title')
    Halaman Edit Data Pemain
@endsection
@section('sub-title')
    Tambah Edit Pemain
@endsection

@section('content')
    

<form action="/cast/{{$casts->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$casts->nama}}" class="form-control">
    </div>
    @error('nama')
      <div class="alert alert-danger">Nama harus diisi</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value="{{$casts->umur}}" class="form-control">
    </div>
    @error('umur')
      <div class="alert alert-danger">Umur harus diisi</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="3">{{$casts->bio}}</textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">Biodata harus diisi</div>
    @enderror
    <button type="submit" class="btn btn-primary">Perbaharui</button>
  </form>
@endsection