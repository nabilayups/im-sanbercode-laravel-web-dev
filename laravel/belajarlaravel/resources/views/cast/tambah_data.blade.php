@extends('layout.master')
@section('title')
    Halaman Tambah Data Pemain
@endsection
@section('sub-title')
    Tambah Data Pemain
@endsection

@section('content')
    

<form action="/cast" method="POST">
  @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
      <div class="alert alert-danger">Nama harus diisi</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" class="form-control">
    </div>
    @error('umur')
      <div class="alert alert-danger">Umur harus diisi</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="3"></textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">Biodata harus diisi</div>
    @enderror
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
@endsection