<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'dashboard']);
Route::get('/register', [AuthController::class,'register']);
Route::post('/welcome', [AuthController::class,'welcome']);
Route::get('/table', [AuthController::class,'table']);
Route::get('/datatables', function(){
    return view('data_table');
});

//Menampilkan list data pemain film
Route::get('/cast', [CastController::class,'index']);

//Menampilka form untuk data pemain baru
Route::get('/cast/create', [CastController::class,'create']);

//Menyimpan data baru ke tabel cast
Route::post('/cast', [CastController::class,'store']);

//Menampilkan detail data pemain film
Route::get('/cast/{cast_id}', [CastController::class,'show']);

//Menampilkan form untuk edit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit', [CastController::class,'edit']);

//Menyimpan perubahan data pemain film atau mengupdate data
Route::put('/cast/{cast_id}', [CastController::class,'update']);

//Menghapus data pemain film
Route::delete('/cast/{cast_id}', [CastController::class,'destroy']);
